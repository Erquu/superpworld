var Movement = {
	NONE: 0,
	LEFT: 1,
	RIGHT: 2
};

function Player() {
	PhysicsObject.call(this);

	this.subY = 0;
	this.move = Movement.NONE;

	this.idleStart = 0;

	this.jump = false;
	this.fall = false;

	this.pipeDown = false;
	this.pipe = null;

	this.halfWidth = 9;
	this.halfHeight = 17.5;

	this.sprite = SpriteLoader.load("sprites/player.json");
	this.sprite.addEventListener("statechange", this);

	this.addEventListener("falling", this);
	this.addEventListener("goup", this);
	this.addEventListener("steptop", this);
	this.addEventListener("stepbottom", this);

	window.addEventListener("keydown", this);
	window.addEventListener("keyup", this);	
}

Player.prototype = Object.create(PhysicsObject.prototype);
Player.prototype.constructor = Player;

Player.prototype.handleEvent = function(ev) {
	//console.log(ev.type);
	switch(ev.type) {
		case "keydown":
			switch (ev.keyCode) {
				case 65: // A
					this.move = Movement.LEFT;
					this.sprite.flip = true;
					if (!(this.jump || this.fall))
						this.sprite.animate("run");
					break;
				case 68: // D
					this.move = Movement.RIGHT;
					this.sprite.flip = false;
					if (!(this.jump || this.fall))
						this.sprite.animate("run");
					break;
				case 87: // W
					if (!this.jump) {
						this.sprite.animate("jump");
						this.velocityY = -200;
					}
					break;
				case 83: // S
					console.log(this.lastCollidedWith);
					if (this.lastCollidedWith instanceof Pipe) {
						this.type = PhysicsObject.STATIC;
						this.pipe = this.lastCollidedWith;
						this.pipeDown = true;
						this.centerX = this.lastCollidedWith.centerX;
					}
					break;
			}
			break;
		case "keyup":
			switch (ev.keyCode) {
				case 65:
				case 68:
					this.move = Movement.NONE;
					if (!(this.jump || this.fall))
						this.sprite.animate("idle");
					break;
			}
			break;
		case "statechange":
			if (ev.state == "idle" && ev.origState.substr(0, 4) != "idle") {
				this.idleStart = ev.srcElement.lastUpdate;
			}
			break;
		case "goup":
			this.jump = true;
			this.sprite.animate("jump");
			break;
		case "falling":
			if (!this.fall) {
				this.fall = true;
				this.sprite.animate("fall");
			}
			break;
		case "steptop":
			this.fall = false;
			this.jump = false;
			this.sprite.animate(this.move == Movement.NONE ? "idle" : "run");
			break;
		case "stepbottom":
		case "stepleft":
		case "stepright":

			break;
	}
};

Player.prototype.update = function(time, step) {

	if (this.pipeDown) {
		this.centerY += 1;
		if (this.getTop() > this.pipe.getTop()) {
			location.href = this.pipe.href;
			window.animate = false;
		}
		return;
	}

	this.sprite.update(time);

	if (this.sprite.activeState == "idle" && time - this.idleStart > 30000) {
		this.idleStart = time;
		this.sprite.animate("idle2");
	}

	switch (this.move) {
		case Movement.LEFT:
			this.centerX -= step * 0.1;
			break;
		case Movement.RIGHT:
			this.centerX += step * 0.1;
			break;
	}
};

Player.prototype.render = function(scene, context, time) {
	var world = scene.toViewport(this.getLeft(), this.getTop());
	this.sprite.render(
		context,
		world.x,
		world.y
	);
};