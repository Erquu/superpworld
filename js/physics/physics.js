function Physics(gravityX, gravityY, worldBounds) {
	this.entities = [];
	this.quadTree = new QuadTree(
		worldBounds.width * 0.5,
		worldBounds.height * 0.5,
		worldBounds.width * 0.5,
		worldBounds.height * 0.5, 0);
	this.resolver = new CollisionResolver();

	this.gravityX = gravityX || 0;
	this.gravityY = gravityY || 0;

	this.lastUpdate = 0;
}

Physics.prototype.constructor = Physics;

Physics.prototype.add = function(obj) {
	if (obj instanceof PhysicsObject) {
		this.entities.push(obj);
	} else {
		console.error("Could not add " + obj);
	}
};

Physics.prototype.step = function(time) {
	var i;

	var elapsed = (time - this.lastUpdate) / 1000;

	this.quadTree.clear();
	for (i = 0, length = this.entities.length; i < length; i++) {
		this.quadTree.insert(this.entities[i]);
	}

	var gravityX = this.gravityX * elapsed;
	var gravityY = this.gravityY * elapsed;
	var entity;

	for (i = 0, length = this.entities.length; i < length; i++) {
		entity = this.entities[i];
		switch (entity.type) {
			case PhysicsObject.DYNAMIC:
				entity.velocityX += entity.accelerationX * elapsed + gravityX;
				entity.velocityY += entity.accelerationY * elapsed + gravityY;
				entity.centerX += entity.velocityX * elapsed;
				entity.centerY += entity.velocityY * elapsed;
				break;
			case PhysicsObject.KINEMATIC:
				entity.velocityX += entity.accelerationX * elapsed;
				entity.velocityY += entity.accelerationY * elapsed;
				entity.centerX += entity.velocityX * elapsed;
				entity.centerY += entity.velocityY * elapsed;
				break;
			case PhysicsObject.STATIC:
				entity.centerX += entity.velocityX * elapsed;
				entity.centerY += entity.velocityY * elapsed;
				break;
		}
	}

	for (i = 0, length = this.entities.length; i < length; i++) {
		var player = this.entities[i];
		if (player.type != PhysicsObject.STATIC) {
			var collisions = this.quadTree.queryRange(player);
			if (collisions.length === 0)
				player.lastCollidedWith = undefined;

			this.resolver.resolve(player, collisions);

			if (player.velocityY > 0) {
				player.fireEvent({type:"falling",srcElement:player,collidedWith:entity});
			} else if (player.velocityY < 0) {
				player.velocityX = 0; //TODO: WHY X?
				player.fireEvent({type:"goup",srcElement:player,collidedWith:entity});
			}

			if (!!player.lastCollidedWith) {
				player.velocityX = player.lastCollidedWith.velocityX;
			}
		}
	}

	this.lastUpdate = time;
};