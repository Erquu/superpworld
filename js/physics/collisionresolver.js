function CollisionResolver() {
	this.THRESHOLD = 0.1;
	this.STICKY_THRESHOLD = 0.3;
}

CollisionResolver.prototype.constructor = CollisionResolver;

CollisionResolver.prototype.resolve = function(player, collisions) {
	for (var i = 0, length = collisions.length; i < length; i++) {
		this.resolveElastic(player, collisions[i]);
	}
};

CollisionResolver.prototype.resolveElastic = function(player, entity) {
	if (player.type == PhysicsObject.STATIC) {
		return;
	}

	var fireEvent = true;

	if (player.lastCollidedWith != entity) {
		player.lastCollidedWith = entity;
	} else {
		fireEvent = false;
	}

	if (fireEvent) {
		player.fireEvent({type:"intersects",srcElement:player,collidedWith:entity});
		entity.fireEvent({type:"intersects",srcElement:entity,collidedWith:player});
	}

	var pMidX = player.centerX;
	var pMidY = player.centerY;
	var aMidX = entity.centerX;
	var aMidY = entity.centerY;

	var dx = (aMidX - pMidX) / entity.halfWidth;
	var dy = (aMidY - pMidY) / entity.halfHeight;

	var absDX = Math.abs(dx);
	var absDY = Math.abs(dy);

	if (Math.abs(absDX - absDY) < this.THRESHOLD) {
		if (dx < 0) {
			player.setLeft(entity.getRight());
			if (fireEvent)
				player.fireEvent({type:"stepright",srcElement:player,collidedWith:entity});
		} else {
			player.setRight(entity.getLeft());
			if (fireEvent)
				player.fireEvent({type:"stepleft",srcElement:player,collidedWith:entity});
		}

		if (dy < 0) {
			player.setTop(entity.getBottom());
			if (fireEvent)
				player.fireEvent({type:"stepbottom",srcElement:player,collidedWith:entity});
		} else {
			player.setBottom(entity.getTop());
			//if (fireEvent)
				player.fireEvent({type:"steptop",srcElement:player,collidedWith:entity});
		}

		if (Math.random() < 0.5) {
			player.velocityX = -player.velocityX * entity.restitution; // Absorb energy and bounce back

			if (Math.abs(player.velocityX) < this.STICKY_THRESHOLD) {
				player.velocityX = 0;
			}
		} else {
			player.velocityY = -player.velocityY * entity.restitution;

			if (Math.abs(player.velocityY) < this.STICKY_THRESHOLD) {
				player.velocityY = 0;
			}
		}
	} else if (absDX > absDY) {
		if (dx < 0) {
			player.setLeft(entity.getRight());
			if (fireEvent)
				player.fireEvent({type:"stepright",srcElement:player,collidedWith:entity});
		} else {
			player.setRight(entity.getLeft());
			if (fireEvent)
				player.fireEvent({type:"stepleft",srcElement:player,collidedWith:entity});
		}

		player.velocityX = -player.velocityX * entity.restitution;

		if (Math.abs(player.velocityX) < this.STICKY_THRESHOLD) {
			player.velocityX = 0;
		}
	} else {
		if (dy < 0) {
			player.setTop(entity.getBottom());
			if (fireEvent)
				player.fireEvent({type:"stepbottom",srcElement:player,collidedWith:entity});
		} else {
			player.setBottom(entity.getTop());
			if (fireEvent)
				player.fireEvent({type:"steptop",srcElement:player,collidedWith:entity});
		}

		player.velocityY = -player.velocityY * entity.restitution;
		if (Math.abs(player.velocityY) < this.STICKY_THRESHOLD) {
			player.velocityY = 0;
		}
	}
};