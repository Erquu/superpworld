function PhysicsObject(collisionName, type, restitution) {
	EventHandler.call(this);

	this.type = type || PhysicsObject.DYNAMIC;
	this.collision = collisionName || PhysicsObject.ELASTIC;

	this.mass = 1.0;
	this.restitution = restitution || 0.2;

	this.centerX = 0;
	this.centerY = 0;
	
	this.halfWidth = 0;
	this.halfHeight = 0;

	this.velocityX = 0;
	this.velocityY = 0;

	this.accelerationX = 0;
	this.accelerationY = 0;

	this.addEventListener("intersects", this);
}

PhysicsObject.prototype = Object.create(EventHandler.prototype);
PhysicsObject.prototype.constructor = PhysicsObject;

// GETTER
PhysicsObject.prototype.getMidX = function() { return this.centerX; };
PhysicsObject.prototype.getMidY = function() { return this.centerY; };
PhysicsObject.prototype.getTop = function() { return this.centerY - this.halfHeight; };
PhysicsObject.prototype.getBottom = function() { return this.centerY + this.halfHeight; };
PhysicsObject.prototype.getLeft = function() { return this.centerX - this.halfWidth; };
PhysicsObject.prototype.getRight = function() { return this.centerX + this.halfWidth; };
PhysicsObject.prototype.getWidth = function() { return this.halfWidth + this.halfWidth; };
PhysicsObject.prototype.getHeight = function() { return this.halfHeight + this.halfHeight; };

// SETTER
PhysicsObject.prototype.setMidX = function(x) { this.centerX = x; };
PhysicsObject.prototype.setMidY = function(y) { this.centerY = y; };
PhysicsObject.prototype.setTop = function(y) { this.centerY = y + this.halfHeight; };
PhysicsObject.prototype.setBottom = function(y) { this.centerY =  y - this.halfHeight; };
PhysicsObject.prototype.setLeft = function(x) { this.centerX = x + this.halfWidth; };
PhysicsObject.prototype.setRight = function(x) { this.centerX = x - this.halfWidth; };
PhysicsObject.prototype.setWidth = function(width) { this.halfWidth = width * 0.5; };
PhysicsObject.prototype.setHeight = function(height) { this.halfHeight = height * 0.5; };

PhysicsObject.prototype.contains = function(x, y) {
	return ((x > this.centerX - this.halfWidth) &&
		(x < this.centerX + this.halfWidth) &&
		(y > this.centerY - this.halfHeight) &&
		(y < this.centerY + this.halfHeight));
};

PhysicsObject.prototype.intersectsWith = function(obj) {
	if (obj instanceof PhysicsObject) {
		var l1 = this.getLeft();
		var t1 = this.getTop();
		var r1 = this.getRight();
		var b1 = this.getBottom();

		var l2 = obj.getLeft();
		var t2 = obj.getTop();
		var r2 = obj.getRight();
		var b2 = obj.getBottom();

		return !(b1 < t2 || t1 > b2 || r1 < l2 || l1 > r2);
	}
	return false;
};

PhysicsObject.prototype.intersectsFull = function(obj) {
	if (obj instanceof PhysicsObject) {
		var l1 = this.getLeft();
		var t1 = this.getTop();
		var r1 = this.getRight();
		var b1 = this.getBottom();

		var l2 = obj.getLeft();
		var t2 = obj.getTop();
		var r2 = obj.getRight();
		var b2 = obj.getBottom();

		return (l1 >= l2 && r1 <= r2 && t1 >= t2 && b1 <= b2);
	}
	return false;
};

PhysicsObject.prototype.handleEvent = function(ev) {
	switch (ev.type) {
		case "intersects":
			ev.srcElement.weight = 0;
			break;
	}
};

// Constants
PhysicsObject.KINEMATIC = "kinematic";
PhysicsObject.DYNAMIC   = "dynamic";
PhysicsObject.DISPLACE  = "displace";
PhysicsObject.ELASTIC   = "elastic";
PhysicsObject.STATIC    = "static";