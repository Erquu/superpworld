var Locations = {
	NW: 0,
	NE: 1,
	SW: 2,
	SE: 3
};

var colors = [
	"red","green","blue","yellow","orange","purple","white","grey","gold"
];

function QuadTree(centerX, centerY, halfWidth, halfHeight, depth) {
	//console.log("QuadTree[centerX:" + centerX + ", centerY:" + centerY + ", halfWidth: " + halfWidth + ", halfHeight:" + halfHeight + ", depth:" + depth + "]")
	PhysicsObject.call(this);

	this.MAX_DEPTH = 5;

	this.debug = false;

	this.centerX = centerX;
	this.centerY = centerY;
	this.halfWidth = halfWidth;
	this.halfHeight = halfHeight;

	this.subTrees = null;
	this.objects = [];

	this.currentDepth = depth;

	if (this.currentDepth < this.MAX_DEPTH) {
		this.divide();
	}
}

QuadTree.prototype = Object.create(PhysicsObject.prototype);
QuadTree.prototype.constructor = QuadTree;

QuadTree.prototype.clear = function() {
	this.objects = [];
	if (!!this.subTrees) {
		for (var i = 0; i < this.subTrees.length; i++) {
			this.subTrees[i].clear();
		}
	}
};

QuadTree.prototype.insert = function(obj) {
	if (obj instanceof PhysicsObject) {
		if (!obj.intersectsFull(this) && this.currentDepth > 0) {
			return false;
		}

		if (this.currentDepth == this.MAX_DEPTH) {
			this.objects.push(obj);
			return true;
		}
		
		for (var i = 0; i < this.subTrees.length; i++) {
			if (this.subTrees[i].insert(obj)) {
				return true;
			}
		}

		this.objects.push(obj);
		return true;
	}
};

QuadTree.prototype.divide = function() {
	if (!!this.subTrees) {
		return;
	}

	var t_centerX = 0;
	var t_centerY = 0;

	var width = this.halfWidth * 0.5;
	var height = this.halfHeight * 0.5;

	this.subTrees = new Array(4);

	t_centerX = this.centerX - width;
	t_centerY = this.centerY - height;
	this.subTrees[Locations.NW] = new QuadTree(t_centerX, t_centerY, width, height, this.currentDepth + 1);

	t_centerX += this.halfWidth;
	this.subTrees[Locations.NE] = new QuadTree(t_centerX, t_centerY, width, height, this.currentDepth + 1);

	t_centerY += this.halfHeight;
	this.subTrees[Locations.SE] = new QuadTree(t_centerX, t_centerY, width, height, this.currentDepth + 1);

	t_centerX -= this.halfWidth;
	this.subTrees[Locations.SW] = new QuadTree(t_centerX, t_centerY, width, height, this.currentDepth + 1);
};

QuadTree.prototype.containsElements = function() {
	var contains = this.objects.length > 0;
	if (!contains && !!this.subTrees) {
		for (var i = 0, length = this.subTrees.length; i < length; i++) {
			contains = contains || this.subTrees[i].containsElements();
		}
	}
	return contains;
};

QuadTree.prototype.debugQueryRange = function(range) {
	var results = [];
	var i, length;

	if (range instanceof PhysicsObject && this.intersectsWith(range)) {
		for (i = 0, length = this.objects.length; i < length; i++) {
			var objCoords = scene.toViewport(this.objects[i].getLeft(), this.objects[i].getTop());
			context.strokeStyle = "yellow";
			context.strokeRect(
				objCoords.x,
				objCoords.y,
				this.objects[i].getWidth(),
				this.objects[i].getHeight()
			);
			if (this.objects[i] != range) {

				var rangeCoords = scene.toViewport(range.centerX, range.centerY);

				context.beginPath();
				context.moveTo(rangeCoords.x, rangeCoords.y);
				context.lineTo(objCoords.x + this.objects[i].halfWidth, objCoords.y + this.objects[i].halfHeight);
				context.stroke();
				context.closePath();

				if (this.objects[i].intersectsWith(range)) {
					context.strokeStyle = "red";
					context.stroke();

					results.push(this.objects[i]);
				}
			}
		}
		if (!!this.subTrees) {
			for (i = 0, length = this.subTrees.length; i < length; i++) {
				results = results.concat(this.subTrees[i].queryRange(range));
			}
		}
	}

	return results;
};

QuadTree.prototype.queryRange = function(range) {
	
	if (this.debug)
		return this.debugQueryRange(range);
	
	var results = [];
	if (range instanceof PhysicsObject && this.intersectsWith(range)) {
		for (var i = 0, length = this.objects.length; i < length; i++) {
			if (this.objects[i] != range) {
				if (this.objects[i].intersectsWith(range)) {
					results.push(this.objects[i]);
				}
			}
		}
		if (!!this.subTrees) {
			for (i = 0, length = this.subTrees.length; i < length; i++) {
				results = results.concat(this.subTrees[i].queryRange(range));
			}
		}
	}

	return results;
};

QuadTree.prototype.render = function(scene, context) {
	var i, length;

	if (this.debug) {
		var world = scene.toViewport(this.centerX, this.centerY);

		if (this.containsElements()) {
			context.strokeStyle = colors[this.currentDepth];
			context.strokeRect(
				world.x - this.halfWidth,
				world.y - this.halfHeight,
				this.halfWidth * 2,
				this.halfHeight * 2
			);

			for (i = 0, length = this.objects.length; i < length; i++) {
				context.beginPath();
				context.moveTo(world.x, world.y);
				var targetCoords = scene.toViewport(this.objects[i].centerX, this.objects[i].centerY);
				context.lineTo(targetCoords.x, targetCoords.y);
				context.stroke();
				context.closePath();
			}
		}
		if (!!this.subTrees) {
			for (i = 0, length = this.subTrees.length; i < length; i++) {
				this.subTrees[i].render(scene, context);
			}
		}
		//context.fillText("Contains: " + (this.containsElements()?"yes":"no"), this.getLeft() + 10, this.getTop() + 30);
	}
};