function Pipe(name, href) {
	PhysicsObject.call(this);

	this.type = PhysicsObject.STATIC;

	this.halfWidth = 24;
	this.halfHeight = 22;

	this.name = name;
	this.href = href;

	this.nameYModifier = Math.random() * 5;

	this.sprite = SpriteLoader.load("sprites/world1-1.json");
	this.sprite.animate("pipe");

	this.halfTextLength = context.measureText(this.name).width * 0.5;
}

Pipe.prototype = Object.create(PhysicsObject.prototype);
Pipe.prototype.constructor = Pipe;

Pipe.prototype.render = function(scene, context, time) {
	var world = scene.toViewport(this.getLeft(), this.getTop());
	var worldCenter = scene.toViewport(this.centerX, 0);

	context.fillStyle = "white";
	context.fillText(
		this.name,
		worldCenter.x - this.halfTextLength,
		world.y - (Math.cos(this.nameYModifier += 0.05) * 10 + 50));

	this.sprite.render(
		context,
		world.x,
		world.y
	);
};