var canvas;
var context;
var windowHeight, windowWidth;

function createCanvas() {
	windowWidth = window.innerWidth;
	windowHeight = window.innerHeight;

	canvas = document.createElement("canvas");
	canvas.width = windowWidth;
	canvas.height = windowHeight;
	context = canvas.getContext("2d");
	document.body.appendChild(canvas);
}