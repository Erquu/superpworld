function SimplePlatform() {	
	PhysicsObject.call(this);

	this.type = PhysicsObject.STATIC;
	this.sprite = SpriteLoader.load("sprites/platform.json");
}

SimplePlatform.prototype = Object.create(PhysicsObject.prototype);
SimplePlatform.prototype.constructor = SimplePlatform;

SimplePlatform.prototype.render = function(scene, context) {
	this.sprite.width = this.getWidth();
	
	var world = scene.toViewport(this.getLeft(), this.getTop());
	this.sprite.render(
		context,
		world.x,
		world.y
	);
};