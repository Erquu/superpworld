window.file_get_contents = function(url) {
	if (window.XMLHttpRequest)
		request = new XMLHttpRequest();
	else
		request = new ActiveXObject("Microsoft.XMLHTTP");
	
	if (request) {
		request.open("GET", url, false);
		request.send(null);
		return request.responseText;
	}
	return false;
};