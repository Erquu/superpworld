(function(global) {
	var clamp = function(value, min, max) {
		return value < min ? min : (value > max ? max : value);
	};


	var Camera = function(viewport, x, y) {
		this._position = {x:x, y:y};
		this._viewport = viewport;
		this._clampBounds = null;
	};

	Camera.prototype = {
		constructor: Camera,
		moveTo: function(x, y) {
			this._position.x = x;
			this._position.y = y;

			var halfWidth = this._viewport.width * 0.5;
			var halfHeight = this._viewport.height * 0.5;

			this._viewport.x = x - halfWidth;
			this._viewport.y = y - halfHeight;

			if (!!this._clampBounds) {
				this._viewport.x = clamp(this._viewport.x, this._clampBounds.x, this._clampBounds.width - this._viewport.width);
				this._viewport.y = clamp(this._viewport.y, this._clampBounds.y, this._clampBounds.height - this._viewport.height);
			}
		},
		clamp: function(bounds) {
			this._clampBounds = bounds;
		},
		translate: function(x, y) {
			this._position.x += x;
			this._position.y += y;
			this._viewport.x += x;
			this._viewport.y += y;
		},
		getViewport: function() {
			return this._viewport;
		},
		setViewport: function(viewport) {
			this._viewport = viewport;
		}
	};

	global.Camera = Camera;

})(this);