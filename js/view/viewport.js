(function(global) {
	var Viewport = function(x, y, width, height) {
		this.x = x || 0;
		this.y = y || 0;
		this.width = width || 0;
		this.height = height || 0;
	};

	Viewport.prototype = {
		constructor: Viewport,
		getBounds: function() {
			return {
				x: this.x,
				y: this.y,
				width: this.width,
				height: this.height
			};
		},
		setBounds: function(bounds) {
			this.x = bounds.x;
			this.y = bounds.y;
			this.width = bounds.width;
			this.height = bounds.height;
		},
		getAspectRatio: function() {
			if (this.height !== 0 && this.width !== 0) {
				return (this.width / this.height);
			}
			return 0;
		}
	};

	global.Viewport = Viewport;
})(this);