(function(global) {
	var Scene = function(bounds) {
		this.viewport = new Viewport(0, 0, bounds.width, bounds.height);
		this.camera = new Camera(this.viewport, bounds.x, bounds.y);
	};

	Scene.prototype = {
		constructor: Scene,
		toWorld: function(x, y) {
			return {
				x: Math.floor(x + this.viewport.x),
				y: Math.floor(y + this.viewport.y)
			};
		},
		toViewport: function(x, y) {
			return {
				x: Math.floor(x - this.viewport.x),
				y: Math.floor(y - this.viewport.y)
			};
		}
	};

	global.Scene = Scene;
})(this);