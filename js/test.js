function Test() {
	PhysicsObject.call(this);
}

Test.prototype = Object.create(PhysicsObject.prototype);
Test.prototype.constructor = Test;

Test.prototype.render = function(context) {
	context.fillStyle = "black";
	context.fillRect(
		this.centerX - this.halfWidth,
		this.centerY - this.halfHeight,
		this.halfWidth * 2,
		this.halfHeight * 2
	);
};