/* HOW TO
 * In die Klasse implementieren
 * EventHandler.call(this);
 * Oder
 * Class.prototype = EventHandler;
 */

if (typeof window.fireEvent !== "function") {
	window.fireEvent = function(callback, args) {
		if (typeof callback === "function") {
			callback(args);
		} else if ((typeof callback === "object") && (typeof callback.handleEvent === "function")) {
			callback.handleEvent(args);
		}
	};
}

function EventHandler() {
	this.eventListeners = [];
}

EventHandler.prototype.constructor = EventHandler;

EventHandler.prototype.addEventListener = function(type, listener, first) {
	if (this.eventListeners[type] === undefined) {
		this.eventListeners[type] = [];
	}
	if (this.eventListeners[type].indexOf(listener) === -1) {
		if (first) {
			this.eventListeners[type] = [listener].concat(this.eventListeners[type]);
		} else {
			this.eventListeners[type].push(listener);
		}
	}
};

EventHandler.prototype.removeEventListener = function(type, listener) {
	var index = this.eventListeners[type].indexOf(listener);
	if (index !== -1) {
		this.eventListeners[type].splice(index, 1);
	}
};

EventHandler.prototype.fireEvent = function(event) {
	for (var listener in this.eventListeners[event.type]) {
		if (event.defaultPrevented) {
			break;
		}
		//console.log(event);
		var func = this.eventListeners[event.type][listener];
		fireEvent(func, event);
	}
};