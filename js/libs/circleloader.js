function CircleLoader(x, y, radius, time, settings) {
	settings = settings || {};
	settings.rotating  = settings.rotating  !== undefined ? settings.rotating  : true;
	settings.color     = settings.color     !== undefined ? settings.color     : '#999';
	settings.lineWidth = settings.lineWidth !== undefined ? settings.lineWidth : 3.0;

	this.rotationAngle = ((Math.PI / 180) * (6 / time));

	this.x = x;
	this.y = y;
	this.radius = radius;

	this.angleSlow = 0;
	this.angleFast = 0;

	this.times = 0;
	this.isRotating = settings.rotating;

	this.color = settings.color;
	this.lineWidth = settings.lineWidth;
}

CircleLoader.prototype.constructor = CircleLoader;

CircleLoader.prototype.update = function() {
	if (this.angleSlow > 6.283185307179586) {
		this.angleSlow = 0;
		this.times = 0;
	}

	if (this.isRotating) {
		this.angleFast += 0.10471975511964; // 6 degrees
	}
	this.angleSlow += this.rotationAngle;

	this.times++;
};

CircleLoader.prototype.render = function(context) {
	context.strokeStyle = this.color;
	context.lineWidth = this.lineWidth;

	context.beginPath();
	context.arc(this.x, this.y, this.radius, this.angleFast, this.angleSlow + this.angleFast, false);
	context.stroke();
	context.closePath();
};