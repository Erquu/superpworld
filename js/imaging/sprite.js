function Sprite() {
	EventHandler.call(this);

	this.id = "undefined";
	this.name = "Undefined";
	this.image = null;
}

Sprite.prototype = Object.create(EventHandler.prototype);
Sprite.prototype.constructor = Sprite;

Sprite.prototype.reset = function() {};
Sprite.prototype.animate = function(state) {};
Sprite.prototype.update = function(time) {};
Sprite.prototype.render = function(context, x, y) {};