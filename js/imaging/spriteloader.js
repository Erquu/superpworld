var SpriteLoader = (function(global) {
	var _imageLibrary = {};

	var SpriteLoader = function SpriteLoader() {
		this.sprites = [];
		this.rootPath = "";
	};

	SpriteLoader.prototype = {
		constructor: SpriteLoader,
		load: function load(path) {
			var json = JSON.parse(file_get_contents(path));
			
			var name = json.name;
			var type = json.type || SpriteLoader.ANIMATED;
			var settings = json.settings || {};
			
			var imagePath = this.rootPath + json.path;
			var image;
			if (_imageLibrary[imagePath] !== undefined) {
				image = _imageLibrary[imagePath];
			} else {
				image = new Image();
				image.src = imagePath;
				image.name = name;
				image.type = type;
				image.addEventListener("load", this);
				_imageLibrary[imagePath] = image;
			}

			var id = name.toLowerCase();

			if (this.sprites[id] === undefined) {
				if (!!json.default) {
					for (var i = 0; i < json.states.length; i++) {
						var state = json.states[i];
						for (var def in json.default) {
							if (!state[def]) {
								state[def] = json.default[def];
							}
						}
					}
				}

				switch (type) {
					case SpriteLoader.ANIMATED:
						this.sprites.push(id);
						this.sprites[id] = new AnimatedSprite(image, settings, json.states);
						break;
					case SpriteLoader.PLATFORM:
						this.sprites.push(id);
						this.sprites[id] = new PlatformSprite(image, settings, json.states);
						break;
				}
				this.sprites[id].id = id;
				this.sprites[id].name = name;
			}

			return this.sprites[id];
		},
		get: function get(index) {
			if (typeof index === "string")
				return this.sprites[index.toLowerCase()];
			else if (typeof index === "number")
				return this.sprites[this.sprites[index]];
		},
		handleEvent: function(ev) {
			var img = ev.srcElement || ev.target;
			console.log("Loaded sprite: " + img.name + " (Type: " + img.type + ")");
		}
	};

	// Constants
	SpriteLoader.ANIMATED = "animated";
	SpriteLoader.PLATFORM = "platform";

	return new SpriteLoader();
})(this);
