var ImageTools = (function() {
	var createSubimage = function createSubimage(image, x, y, w, h) {
		var canvas = document.createElement("canvas");
		document.body.appendChild(canvas);
		canvas.width = w;
		canvas.height = h;
		var ctx = canvas.getContext("2d");

		ctx.drawImage(image, x, y, w, h, 0, 0, w, h);
		var newImageUrl = canvas.toDataURL("image/png");
		var newImage = new Image();
		newImage.src = newImageUrl;

		console.log(newImageUrl);

		return newImage;
	};

	var flipImage = function flipImage(image, scope, callback) {
		image.addEventListener("load", function(ev) {
			var w = image.width;
			var h = image.height;

			var canvas = document.createElement("canvas");
			canvas.width = w;
			canvas.height = h;
			//document.body.appendChild(canvas);

			var ctx = canvas.getContext("2d");
			ctx.save();
			ctx.scale(-1, 1);
			//ctx.drawImage(image, 0, 0, -w, h);
			ctx.drawImage(image, 0, 0, w, h);
			ctx.restore();

			var newImageUrl = canvas.toDataURL("image/png");
			var newImage = new Image();
			newImage.src = newImageUrl;

			callback(scope, newImage);
		});
	};

	var flipImageDeep = function(image, scope, callback) {
		image.addEventListener("load", function(ev) {
			var w = image.width;
			var h = image.height;

			var canvas = document.createElement("canvas");
			canvas.width = w;
			canvas.height = h;
			//document.body.appendChild(canvas);

			var ctx = canvas.getContext("2d");
			ctx.drawImage(image, 0, 0, w, h);
			var imageData = ctx.getImageData(0, 0, w, h);
			var newImageData = ctx.createImageData(w, h);

			var line = w * 4;

			for (var i = 0, l = imageData.data.length; i < l; i += 4) {
				var r = imageData.data[i];
				var g = imageData.data[i+1];
				var b = imageData.data[i+2];
				var a = imageData.data[i+3];

				var currentLine = Math.ceil(i / line);
				px = line * currentLine - (i % line) - 4;

				newImageData.data[px] = r;
				newImageData.data[px+1] = g;
				newImageData.data[px+2] = b;
				newImageData.data[px+3] = a;
			}

			ctx.putImageData(newImageData, 0, 0);

			var newImageUrl = canvas.toDataURL("image/png");
			var newImage = new Image();
			newImage.src = newImageUrl;

			callback(scope, newImage);
		});
	};

	return {
		createSubimage: createSubimage,
		flipImage: flipImageDeep
	};
})();