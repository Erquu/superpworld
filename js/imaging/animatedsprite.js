function AnimatedSprite(image, settings, lines) {
	Sprite.call(this);

	this.states = [];
	this.image = image;

	this.flippedImage = null;
	if (settings.prerenderflip === true) {
		ImageTools.flipImage(image, this, function(self, img) {
			self.flippedImage = img;
		});
	}
	
	this.activeState = null;
	this.activeLine = null;
	this.duration = 0;
	this.stateFrame = 0;
	this.lastUpdate = 0;

	this.flip = false;

	for (var i = 0; i < lines.length; i++) {
		var line = lines[i];

		if (!!line.inherit && !!this.states[line.inherit]) {
			for (var test in this.states[line.inherit]) {
				if (line[test] === undefined) {
					line[test] = this.states[line.inherit][test];
				}
			}
		}

		if (!this.activeState) {
			this.activeState = line.name.toLowerCase();
			this.activeLine = line;
		}
		if (line.repeat === undefined) {
			line.repeat = true;
		}

		this.states.push(line.name.toLowerCase());
		this.states[line.name.toLowerCase()] = line; // {name:"Name",linenum:Int,length:Int}
	}
}

AnimatedSprite.prototype = Object.create(Sprite.prototype);
AnimatedSprite.prototype.constructor = AnimatedSprite;

AnimatedSprite.prototype.reset = function() {
	this.stateFrame = 0;
	this.duration = 0;
};

AnimatedSprite.prototype.animate = function(state) {
	if (typeof state === "string") {
		state = state.toLowerCase();
	} else if (typeof state === "number") {
		if (state >= 0 && state < this.states.length) {
			state = this.states[state];
		}
	}

	if (state != this.activeState && !!this.states[state]) {
		this.fireEvent({type:"statechange",origState:this.activeState,state:state,srcElement:this});
		this.reset();
		this.activeLine = this.states[state];
		this.activeState = state;
	}
};

AnimatedSprite.prototype.update = function(time) {
	if (time > this.lastUpdate + this.activeLine.pause) {
		this.lastUpdate = time;

		if (this.stateFrame + 1 < this.activeLine.length) { // Animation did not reach it's end yet
			this.stateFrame++;
		} else {
			this.duration++;

			if (!!this.activeLine.durations) {
				if (this.duration < this.activeLine.durations) { // Duration is enabled and not yet done
					this.stateFrame = 0; // Repeat
					return;
				} else {
					if (!!this.activeLine.next) {
						this.animate(this.activeLine.next);
					}
				}
			} else if (!!this.activeLine.next) {
				this.animate(this.activeLine.next);
			} else if (this.activeLine.repeat) {
				this.stateFrame = 0;
			}
		}
	}
};

AnimatedSprite.prototype.render = function(context, x, y) {
	if (!!this.activeLine.x)
		x += this.activeLine.x;
	if (!!this.activeLine.y)
		y += this.activeLine.y;
	
	if (!this.flip)
		this.draw(context, x, y);
	else
		this.drawFlipped(context, x, y);
};

AnimatedSprite.prototype.draw = function(context, x, y) {
	context.drawImage(
		this.image,
		this.stateFrame * this.activeLine.sizex,
		this.activeLine.starty,
		this.activeLine.sizex,
		this.activeLine.sizey,
		x,
		y,
		this.activeLine.sizex,
		this.activeLine.sizey);
};

AnimatedSprite.prototype.drawFlipped = function(context, x, y) {
	if (this.flippedImage !== null) {
		var w = this.flippedImage.width;
		context.drawImage(
			this.flippedImage,
			w - (this.stateFrame * this.activeLine.sizex) - this.activeLine.sizex,
			this.activeLine.starty,
			this.activeLine.sizex,
			this.activeLine.sizey,
			x,
			y,
			this.activeLine.sizex,
			this.activeLine.sizey);
	} else {
		ImageTools.flipImage(image, this, function(self, img) {
			self.flippedImage = img;
		});
	}
};