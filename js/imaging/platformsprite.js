function PlatformSprite(image, settings, lines) {
	Sprite.call(this);

	this.image = image;
	this.left = undefined;
	this.right = undefined;
	this.center = undefined;

	this.width = 0;

	for (var i = 0; i < lines.length; i++) {
		var line = lines[i];
		switch(line.name) {
			case "left":
				this.left = line;
				break;
			case "middle":
				this.center = line;
				break;
			case "right":
				this.right = line;
				break;
		}
	}
}

PlatformSprite.prototype = Object.create(Sprite.prototype);
PlatformSprite.prototype.constructor = PlatformSprite;

PlatformSprite.prototype.render = function(context, x, y) {
	var addX = 0;
	var subX = 0;
	
	if (!!this.left) {
		subX += this.left.sizex;
		addX += this.left.sizex;

		context.drawImage(
			this.image,
			this.left.startx,
			this.left.starty,
			this.left.sizex,
			this.left.sizey,
			x,
			y,
			this.left.sizex,
			this.left.sizey
		);
	}
	if (!!this.right) {
		subX += this.right.sizex;
	}

	if (!!this.center) {
		var times = (this.width - subX) / this.center.sizex;
		var timesFloored = Math.floor(times);
		times -= timesFloored;

		for (var i = 0; i < timesFloored; i++) {
			context.drawImage(
				this.image,
				this.center.startx,
				this.center.starty,
				this.center.sizex,
				this.center.sizey,
				x + addX,
				y,
				this.center.sizex,
				this.center.sizey
			);
			addX += this.center.sizex;
		}
		context.drawImage(
			this.image,
			this.center.startx,
			this.center.starty,
			this.center.sizex * times,
			this.center.sizey,
			x + addX,
			y,
			this.center.sizex * times,
			this.center.sizey
		);
	}

	if (!!this.right) {
		context.drawImage(
			this.image,
			this.right.startx,
			this.right.starty,
			this.right.sizex,
			this.right.sizey,
			x + this.width - this.right.sizex,
			y,
			this.right.sizex,
			this.right.sizey
		);
	}
};